
let emotes = {
  'A': 'img/865.png', // holy kalvoda
  'B': 'img/938.png', // vagner approved
  'C': 'img/575.png', // vagner stationary
  'D': 'img/355.png', // stary wut
  'E': 'img/146.png', // vagner clown
  'F': 'img/436.png', // vagner angery
  ' ': '',
  '': ''
}

function discordify(node) {
  let grade = node.textContent,
      img = emotes[grade[grade.length - 1]]
  
  if(!img) {
    return
  }

  node.title = "[známka " + grade + "] " + node.title
  node.textContent = ""
  node.innerHTML = "<img src='" + browser.runtime.getURL(img) + "' width='32' height='32'/>"
}


function KOS() {
  let vysledkyPage = [
    ...document.getElementsByClassName('tableRow1'), 
    ...document.getElementsByClassName('tableRow2')
  ].filter(x => x.children.length === 12)

  vysledkyPage.forEach(x => {
    if(x.children[10].innerHTML.length === 1) {
      discordify(x.children[10])
    }
  })

  let pruchodPage = [
    ...document.getElementsByClassName('tableRow4')
  ].filter(x => x.children.length === 16)

  pruchodPage.forEach(x => {
    if(x.children[10].innerHTML.length === 7) {
      discordify(x.children[10])
    }
  })
}

function Grades() {
  let elements = [
    ...document.getElementsByClassName('score-wrapper')
  ]

  elements.forEach(x => {
    discordify(x.children[0])
  })
}


KOS()
Grades()
setTimeout(()=>{
  Grades()
}, 600)
