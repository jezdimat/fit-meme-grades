# FIT Emoji grades

Líbí se vám vaše známky? Taky ne? Tak se jim alespoň vysmějte.

Projekt adaptován z webextensions-examples-master/emoji-substitution od týmu Firefox. Kód v index.js byl ale sepsán od nuly (taky tak ještě vypadá, je to moje první extention)

## Instalace

Firefox extention:
- naklonujte si projekt gitem / stáhněte a rozbalte zip
- otevřete v prohlžeči about:debugging
- přepněte se na This Firefox
- Add temporary extention
- vyberte soubor manifest.json ze staženého repozitáře

Chrome extentions:
- naklonujte si projekt gitem / stáhněte a rozbalte zip
- otevřete v prohlížeči chrome://extensions/
- zapněte Developer settings
- Load upacked
- vyberte složku obsahující soubor manifest.json ze staženého repozitáře

## Specificky

Známka | Klasicky
------ | -----------------
A      | ![x](img/865.png)
B      | ![x](img/938.png)
C      | ![x](img/575.png)
D      | ![x](img/355.png)
E      | ![x](img/146.png)
F      | ![x](img/436.png)

## Todos

- ! Grades vyžadují pokaždé reload pro načtení ikonek
- Přepsat kód do pořádné podoby
- Udělat readme anglicky
